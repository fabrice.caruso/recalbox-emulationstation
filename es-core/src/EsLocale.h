#ifndef _LOCALE_H_
#define _LOCALE_H_

#include <boost/locale.hpp>

#ifdef WIN32

const std::string convertFromWideString(const std::wstring wstring);
const std::wstring convertToWideString(const std::string string);

#define UNICODE_CHARTYPE wchar_t*
#define UNICODE_CHARS(x) L ## x
#define UNICODE_STRING(x) convertFromWideString(L ## x)

#define _(A) boost::locale::gettext(A)
#define ngettext(A, B, C) boost::locale::ngettext(A, B, C)

#else

#define _(A) boost::locale::gettext(A)
#define ngettext(A, B, C) boost::locale::ngettext(A, B, C)

#define UNICODE_CHARTYPE char*
#define UNICODE_CHARS(x) x
#define UNICODE_STRING(x) x
#endif

#endif
