#pragma once

#include <string>

#include <boost/date_time.hpp>

#include "math/Transform4x4f.h"
#include "math/Vector3f.h"
#include "math/Vector2f.h"

#ifdef WIN32
	#include <time.h>
	#include <Windows.h>
	#include <winsock.h>
	#include <stdint.h> // portable: uint64_t   MSVC: __int64 

	#define CLOCK_REALTIME 0

	int gettimeofday(struct timeval * tp, struct timezone * tzp);
	int clock_gettime(int, struct timespec *spec);
	DWORD getppid();
#else
	#include <sys/time.h>
#endif

void strFindAndReplace(std::string& source, const std::string& find, const std::string& replace);

std::string strToUpper(const char* from);
std::string& strToUpper(std::string& str);
std::string strToUpper(const std::string& str);

Transform4x4f& roundMatrix(Transform4x4f& mat);
Transform4x4f roundMatrix(const Transform4x4f& mat);

Vector3f roundVector(const Vector3f& vec);
Vector2f roundVector(const Vector2f& vec);

std::string getExpandedPath(const std::string& str);

boost::posix_time::ptime string_to_ptime(const std::string& str, const std::string& fmt = "%Y%m%dT%H%M%S%F%q");
