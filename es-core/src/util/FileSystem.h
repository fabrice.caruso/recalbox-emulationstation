#pragma once

#include <list>
#include <string>
#include <sys/stat.h>

#if defined(_WIN32)
// because windows...
#include <direct.h>
#include <Windows.h>
#define getcwd _getcwd
#define mkdir(x,y) _mkdir(x)
//#define snprintf _snprintf
#define stat64 _stat64
#define unlink _unlink
#define S_ISREG(x) (((x) & S_IFMT) == S_IFREG)
#define S_ISDIR(x) (((x) & S_IFMT) == S_IFDIR)

#ifdef _MSC_VER
#ifndef PATH_MAX
#define PATH_MAX 260
#endif
#endif

#else // _WIN32
#include <dirent.h>
#include <unistd.h>
#endif // _WIN32

namespace StringUtil
{
	bool startsWith(const std::string& str, const std::string& prefix);
	bool endsWith(const std::string& str, const std::string& suffix);
}

namespace FileSystem
{
	std::string removeCommonPathUsingStrings(const std::string& path, const std::string& relativeTo, bool& contains);
	std::string removeCommonPath(const std::string& path, const std::string& relativeTo, bool& contains);

	std::string temp_directory_path();
	std::string escape_path(const std::string& path);
	std::string make_prefered(const std::string& path);
	bool create_directory(const std::string& _path);
	bool is_symlink(const std::string& _path);

	std::string get_extension(const std::string& _path);
	bool exists(const std::string& path);
	std::string get_generic_path(const std::string& _path);
	bool is_directory(const std::string& _path);
	bool is_regular_file(const std::string& _path);
	bool is_hidden(const std::string& _path);
	std::string get_filename(const std::string& _path);
	std::string get_parent_path(const std::string& _path);
	std::string combine(const std::string& _path, const std::string& filename);
	std::string stem(const std::string& _path);
	std::string get_canonical_path(const std::string& _path);
	std::string get_cwd();
	std::string get_absolute_path(const std::string& _path, const std::string& _base = get_cwd());
	std::list<std::string> get_split_path(const std::string& _path);
	bool        delete_file(const std::string& _path);

	std::string resolveRelativePath(const std::string& _path, const std::string& _relativeTo, const bool _allowHome);
	std::string makeRelativePath(const std::string& path, const std::string& relativeTo, bool allowHome);

	std::list<std::string>  get_files(const std::string& _path, bool recursive = false, const bool includeHidden = true);
	std::list<std::string>  get_directories(const std::string& _path, bool recursive = false, const bool includeHidden = true);

	class Filepath
	{
	public:
		Filepath(std::string s) : mPathname(s) { }

		Filepath& operator=(const Filepath& p)
		{
			mPathname = p.mPathname;
			return *this;
		}

		Filepath& operator=(std::string s) { mPathname = s; return *this; }

		std::string string() { return mPathname; }

		std::string generic_string() { return FileSystem::get_generic_path(mPathname); }

		Filepath filename() { return get_filename(mPathname); }
		Filepath parent_path() { return FileSystem::get_parent_path(mPathname); }
		Filepath stem() { return FileSystem::stem(mPathname); }

		bool exists() { return FileSystem::exists(mPathname); }

	protected:
		std::string mPathname;
	};
}