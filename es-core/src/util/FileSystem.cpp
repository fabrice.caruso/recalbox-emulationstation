#include "FileSystem.h"
#include "platform.h"

#ifdef _WIN32
#include "EsLocale.h"
#endif

namespace StringUtil
{
	bool startsWith(const std::string& str, const std::string& prefix)
	{
		return str.size() >= prefix.size() && 0 == str.compare(0, prefix.size(), prefix);
	}

	bool endsWith(const std::string& str, const std::string& suffix)
	{
		return str.size() >= suffix.size() && 0 == str.compare(str.size() - suffix.size(), suffix.size(), suffix);
	}
}

namespace FileSystem
{
	std::string makeRelativePath(const std::string& path, const std::string& relativeTo, bool allowHome)
	{
		bool contains = false;

		std::string ret = removeCommonPath(path, relativeTo, contains);
		if (contains)
		{
			// success
			ret = combine(".", ret);
			return ret;
		}

		if (allowHome)
		{
			ret = removeCommonPath(path, getHomePath(), contains);
			if (contains)
			{
				// success
				ret = combine("~", ret);
				return ret;
			}
		}

		// nothing could be resolved
		return path;
	}

	// example: removeCommonPath("/home/pi/roms/nes/foo/bar.nes", "/home/pi/roms/nes/") returns "foo/bar.nes"
	std::string removeCommonPath(const std::string& path, const std::string& relativeTo, bool& contains)
	{
		// if either of these doesn't exist, fs::canonical() is going to throw an error
		if (!FileSystem::exists(path) || !FileSystem::exists(relativeTo))
		{
			contains = false;
			return path;
		}

		auto p = FileSystem::get_split_path(path);
		auto r = FileSystem::get_split_path(relativeTo);

		if (p.size() > 0 && r.size() > 0 && p.front() != r.front())
		{
			contains = false;
			return path;
		}

		std::string result;

		// find point of divergence
		auto itr_path = p.begin();
		auto itr_relative_to = r.begin();
		while (itr_path != p.end() && itr_relative_to != r.end())
			if (*itr_path == *itr_relative_to)
			{
				++itr_path;
				++itr_relative_to;
			}
			else
				break;

		if (itr_relative_to != r.end())
		{
			contains = false;
			return path;
		}

		while (itr_path != p.end())
		{
			if (*itr_path != ".")
				result = FileSystem::combine(result, *itr_path);

			++itr_path;
		}
	
		contains = true;
		return result;
	}

	std::string removeCommonPathUsingStrings(const std::string& path, const std::string& relativeTo, bool& contains)
	{
		std::string pathStr = get_generic_path(path);
		std::string relativeToStr = get_generic_path(relativeTo);

		if (pathStr.compare(0, relativeToStr.length(), relativeToStr) == 0)
		{
			contains = true;
			return pathStr.substr(relativeToStr.size() + 1);
		}

		contains = false;
		return path;		
	}


	std::string get_extension(const std::string& _path)
	{
		std::string fileName = get_filename(_path);
		size_t      offset = std::string::npos;

		// empty fileName
		if (fileName == ".")
			return fileName;

		// find last '.' and return the extension
		if ((offset = fileName.find_last_of('.')) != std::string::npos)
			return std::string(fileName, offset);

		// no '.' found, filename has no extension
		return "";
	}

	std::string temp_directory_path()
	{
#ifdef WIN32
		TCHAR lpTempPathBuffer[MAX_PATH];

		DWORD dwRetVal = ::GetTempPath(MAX_PATH, lpTempPathBuffer);
		if (dwRetVal > MAX_PATH || (dwRetVal == 0))
			return "";

		return std::string(lpTempPathBuffer);
#else
		const char* val = 0;

		(val = std::getenv("TMPDIR")) ||
			(val = std::getenv("TMP")) ||
			(val = std::getenv("TEMP")) ||
			(val = std::getenv("TEMPDIR"));

#ifdef __ANDROID__
		const char* default_tmp = "/data/local/tmp";
#else
		const char* default_tmp = "/tmp";
#endif
		std::string p((val != 0) ? val : default_tmp);
		return p;
#endif	
	}

	bool delete_file(const std::string& _path)
	{
		std::string path = get_generic_path(_path);

		// don't remove if it doesn't exists
		if (!exists(path))
			return true;

		// try to remove file
		return (unlink(path.c_str()) == 0);

	} // removeFile


	std::string resolveRelativePath(const std::string& _path, const std::string& _relativeTo, const bool _allowHome)
	{
		std::string path = get_generic_path(_path);
		std::string relativeTo = is_directory(_relativeTo) ? get_generic_path(_relativeTo) : get_parent_path(_relativeTo);

		// nothing to resolve
		if (!path.length())
			return path;

		// replace '.' with relativeTo
		if ((path[0] == '.') && (path[1] == '/'))
			return combine(relativeTo, &(path[1]));

		// replace '~' with homePath
		if (_allowHome && (path[0] == '~') && (path[1] == '/'))
			return combine(getHomePath(), &(path[1]));

		// nothing to resolve
		return path;
	}

	std::list<std::string> get_files(const std::string& _path, const bool recursive, const bool includeHidden)
	{
		std::string path = get_generic_path(_path);
		std::list<std::string>  contentList;

		// only parse the directory, if it's a directory
		if (is_directory(path))
		{
#ifdef _WIN32
			WIN32_FIND_DATAW findData;
			std::string      wildcard = path + "/*";
			HANDLE           hFind = FindFirstFileW(std::wstring(wildcard.begin(), wildcard.end()).c_str(), &findData);

			if (hFind != INVALID_HANDLE_VALUE)
			{
				// loop over all files in the directory
				do
				{
					std::string name = convertFromWideString(findData.cFileName);

					// ignore "." and ".."
					if ((name != ".") && (name != ".."))
					{
						std::string fullName(get_generic_path(path + "/" + name));

						if (!includeHidden && (findData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == FILE_ATTRIBUTE_HIDDEN)
							continue;

						if (recursive && (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
							contentList.merge(get_files(fullName, true, includeHidden));
						else 
							contentList.push_back(fullName);
					}
				} while (FindNextFileW(hFind, &findData));

				FindClose(hFind);
			}
#else // WIN32
			DIR* dir = opendir(path.c_str());

			if (dir != NULL)
			{
				struct dirent* entry;

				// loop over all files in the directory
				while ((entry = readdir(dir)) != NULL)
				{
					std::string name(entry->d_name);

					// ignore "." and ".."
					if ((name != ".") && (name != ".."))
					{
						std::string fullName(get_generic_path(path + "/" + name));

						if (!includeHidden && FileSystem::is_hidden(fullName))
							continue;

						if (recursive && is_directory(fullName))
							contentList.merge(get_files(fullName, true, includeHidden));
						else 
							contentList.push_back(fullName);
					}
				}

				closedir(dir);
			}
#endif // WIN32
		}
		
		return contentList;
	}

	std::list<std::string> get_directories(const std::string& _path, const bool recursive, const bool includeHidden)
	{
		std::string path = get_generic_path(_path);
		std::list<std::string>  contentList;

		// only parse the directory, if it's a directory
		if (is_directory(path))
		{
#ifdef _WIN32
			WIN32_FIND_DATAW findData;
			std::string      wildcard = path + "/*";
			HANDLE           hFind = FindFirstFileW(std::wstring(wildcard.begin(), wildcard.end()).c_str(), &findData);

			if (hFind != INVALID_HANDLE_VALUE)
			{
				// loop over all files in the directory
				do
				{
					std::string name = convertFromWideString(findData.cFileName);

					// ignore "." and ".."
					if ((name != ".") && (name != ".."))
					{
						std::string fullName(get_generic_path(path + "/" + name));

						if (!includeHidden && (findData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == FILE_ATTRIBUTE_HIDDEN)
							continue;

						if ((findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
						{
							contentList.push_back(fullName);

							if (recursive)
								contentList.merge(get_directories(fullName, true, includeHidden));
						}

					}
				} while (FindNextFileW(hFind, &findData));

				FindClose(hFind);
			}
#else // WIN32
			DIR* dir = opendir(path.c_str());

			if (dir != NULL)
			{
				struct dirent* entry;

				// loop over all files in the directory
				while ((entry = readdir(dir)) != NULL)
				{
					std::string name(entry->d_name);

					// ignore "." and ".."
					if ((name != ".") && (name != ".."))
					{
						std::string fullName(get_generic_path(path + "/" + name));

						if (!includeHidden && FileSystem::is_hidden(fullName))
							continue;

						if (is_directory(fullName))
						{
							contentList.push_back(fullName);
							if (recursive)
								contentList.merge(get_files(fullName, true, includeHidden));
						}
					}
				}

				closedir(dir);
			}
#endif // WIN32
		}

		return contentList;
	}

	std::list<std::string> get_split_path(const std::string& _path)
	{
		std::list<std::string>  pathList;
		std::string path = get_generic_path(_path);
		size_t      start = 0;
		size_t      end = 0;

		// split at '/'
		while ((end = path.find("/", start)) != std::string::npos)
		{
			if (end != start)
				pathList.push_back(std::string(path, start, end - start));

			start = end + 1;
		}

		// add last folder / file to pathList
		if (start != path.size())
			pathList.push_back(std::string(path, start, path.size() - start));

		// return the path list
		return pathList;

	}

	std::string stem(const std::string& _path)
	{
		std::string fileName = get_filename(_path);
		size_t      offset = std::string::npos;

		// empty fileName
		if (fileName == ".")
			return fileName;

		// find last '.' and erase the extension
		if ((offset = fileName.find_last_of('.')) != std::string::npos)
			return fileName.erase(offset);

		// no '.' found, filename has no extension
		return fileName;
	}

	std::string combine(const std::string& _path, const std::string& filename)
	{
		std::string gp = get_generic_path(_path);


		if (StringUtil::startsWith(filename, "/.."))
		{
			auto f = get_split_path(filename);		
			
			int count = 0;
			for (auto it = f.cbegin(); it != f.cend(); ++it)
			{
				if (*it != "..")
					break;

				count++;
			}

			if (count > 0)
			{
				auto list = get_split_path(gp);
				std::vector<std::string> p(list.begin(), list.end());
				
				std::string result;

				for (int i = 0; i < p.size() - count; i++)
				{
					if (result.empty())
						result = p.at(i);
					else
						result = result + "/" + p.at(i);
				}

				std::vector<std::string> fn(f.begin(), f.end());
				for (int i = count; i < fn.size(); i++)
				{
					if (result.empty())
						result = fn.at(i);
					else
						result = result + "/" + fn.at(i);
				}

				return result;
			}
		}


		if (!StringUtil::endsWith(gp, "/") && !StringUtil::endsWith(gp, "\\"))
			if (!StringUtil::startsWith(filename, "/") && !StringUtil::startsWith(filename, "\\"))
				gp += "/";

		return gp + filename;
	}

	bool create_directory(const std::string& _path)
	{
		std::string path = get_generic_path(_path);

		// don't create if it already exists
		if (exists(path))
			return true;

		// try to create directory
		if (mkdir(path.c_str(), 0755) == 0)
			return true;

		// failed to create directory, try to create the parent
		std::string parent = get_parent_path(path);

		// only try to create parent if it's not identical to path
		if (parent != path)
			create_directory(parent);

		// try to create directory again now that the parent should exist
		return (mkdir(path.c_str(), 0755) == 0);

	} // createDirectory

	bool exists(const std::string& path)
	{
#ifdef WIN32
		std::wstring gp = convertToWideString(make_prefered(get_generic_path(path)));
		struct stat64 info;

		// check if stat64 succeeded
		return (_wstat64(gp.c_str(), &info) == 0);		
#else
		//convertToWideString(path);
		std::string gp = make_prefered(get_generic_path(path));
		struct stat64 info;

		// check if stat64 succeeded
		return (stat64(gp.c_str(), &info) == 0);
#endif
	}	

	bool is_regular_file(const std::string& _path)
	{
		std::string path = get_generic_path(_path);
		struct stat64 info;

		// check if stat64 succeeded
		if (stat64(path.c_str(), &info) != 0)
			return false;

		// check for S_IFREG attribute
		return (S_ISREG(info.st_mode));
	}

	bool is_directory(const std::string& _path)
	{
		std::string path = get_generic_path(_path);
		struct stat info;

		// check if stat succeeded
		if (stat(path.c_str(), &info) != 0)
			return false;

		// check for S_IFDIR attribute
		return (S_ISDIR(info.st_mode));

	} // isDirectory

	bool is_hidden(const std::string& _path)
	{
		std::string path = get_generic_path(_path);

#if defined(_WIN32)
		// check for hidden attribute
		const DWORD Attributes = GetFileAttributes(path.c_str());
		if ((Attributes != INVALID_FILE_ATTRIBUTES) && (Attributes & FILE_ATTRIBUTE_HIDDEN))
			return true;
#endif // _WIN32

		// filenames starting with . are hidden in linux, we do this check for windows as well
		if (get_filename(path)[0] == '.')
			return true;

		// not hidden
		return false;
	}

	std::string get_filename(const std::string& _path)
	{
		std::string path = get_generic_path(_path);
		size_t      offset = std::string::npos;

		// find last '/' and return the filename
		if ((offset = path.find_last_of('/')) != std::string::npos)
			return ((path[offset + 1] == 0) ? "." : std::string(path, offset + 1));

		// no '/' found, entire path is a filename
		return path;
	}

	std::string get_parent_path(const std::string& _path)
	{
		std::string path = get_generic_path(_path);
		size_t      offset = std::string::npos;

		// find last '/' and erase it
		if ((offset = path.find_last_of('/')) != std::string::npos)
			return path.erase(offset);

		// no parent found
		return path;
	}

	std::string get_generic_path(const std::string& _path)
	{
		std::string path = _path;
		size_t      offset = std::string::npos;

		// remove "\\\\?\\"
		if ((path.find("\\\\?\\")) == 0)
			path.erase(0, 4);

		// convert '\\' to '/'
		while ((offset = path.find('\\')) != std::string::npos)
			path.replace(offset, 1, "/");

		// remove double '/'
		while ((offset = path.find("//")) != std::string::npos)
			path.erase(offset, 1);

		// remove trailing '/'
		while (path.length() && ((offset = path.find_last_of('/')) == (path.length() - 1)))
			path.erase(offset, 1);

		// return generic path
		return path;
	}

	bool is_symlink(const std::string& _path)
	{
		std::string path = get_generic_path(_path);

#if defined(_WIN32)
		// check for symlink attribute
		const DWORD Attributes = GetFileAttributes(path.c_str());
		if ((Attributes != INVALID_FILE_ATTRIBUTES) && (Attributes & FILE_ATTRIBUTE_REPARSE_POINT))
			return true;
#else
		struct stat info;

		// check if lstat succeeded
		if (lstat(path.c_str(), &info) != 0)
			return false;

		// check for S_IFLNK attribute
		return (S_ISLNK(info.st_mode));
#endif

		return false;
	}

	bool is_absolute_path(const std::string& _path)
	{
		std::string path = get_generic_path(_path);

#if defined(_WIN32)
		return ((path.size() > 1) && (path[1] == ':'));
#else
		return ((path.size() > 0) && (path[0] == '/'));
#endif
	}

	std::string get_cwd()
	{
		char temp[512];
		return (getcwd(temp, 512) ? get_generic_path(temp) : "");
	}

	std::string get_absolute_path(const std::string& _path, const std::string& _base)
	{
		std::string path = get_generic_path(_path);
		std::string base = is_absolute_path(_base) ? get_generic_path(_base) : get_absolute_path(_base);
		return is_absolute_path(path) ? path : get_generic_path(base + "/" + path);
	}

	std::string resolve_symlink(const std::string& _path)
	{
		std::string path = get_generic_path(_path);
		std::string resolved;

#if defined(_WIN32)
		HANDLE hFile = CreateFile(path.c_str(), FILE_READ_ATTRIBUTES, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, 0);

		if (hFile != INVALID_HANDLE_VALUE)
		{
			resolved.resize(GetFinalPathNameByHandle(hFile, nullptr, 0, FILE_NAME_NORMALIZED) + 1);
			if (GetFinalPathNameByHandle(hFile, (LPSTR)resolved.data(), (DWORD)resolved.size(), FILE_NAME_NORMALIZED) > 0)
			{
				resolved.resize(resolved.size() - 1);
				resolved = get_generic_path(resolved);
			}
			CloseHandle(hFile);
		}
#else // _WIN32
		struct stat info;

		// check if lstat succeeded
		if (lstat(path.c_str(), &info) == 0)
		{
			resolved.resize(info.st_size);
			if (readlink(path.c_str(), (char*)resolved.data(), resolved.size()) > 0)
				resolved = get_generic_path(resolved);
		}
#endif // _WIN32

		// return resolved path
		return resolved;

	} // resolveSymlink

	std::string get_canonical_path(const std::string& _path)
	{
		// temporary hack for builtin resources
		if ((_path[0] == ':') && (_path[1] == '/'))
			return _path;

		std::string path = exists(_path) ? get_absolute_path(_path) : get_generic_path(_path);

		// cleanup path
		bool scan = true;
		while (scan)
		{
			auto pathList = get_split_path(path);

			path.clear();
			scan = false;

			for (auto it = pathList.cbegin(); it != pathList.cend(); ++it)
			{
				// ignore empty
				if ((*it).empty())
					continue;

				// remove "/./"
				if ((*it) == ".")
					continue;

				// resolve "/../"
				if ((*it) == "..")
				{
					path = get_parent_path(path);
					continue;
				}

#if defined(_WIN32)
				// append folder to path
				path += (path.size() == 0) ? (*it) : ("/" + (*it));
#else // _WIN32
				// append folder to path
				path += ("/" + (*it));
#endif // _WIN32

				// resolve symlink
				if (is_symlink(path))
				{
					std::string resolved = resolve_symlink(path);

					if (resolved.empty())
						return "";

					if (is_absolute_path(resolved))
						path = resolved;
					else
						path = get_parent_path(path) + "/" + resolved;

					for (++it; it != pathList.cend(); ++it)
						path += (path.size() == 0) ? (*it) : ("/" + (*it));

					scan = true;
					break;
				}
			}
		}

		// return canonical path
		return path;
	}

		
	std::string make_prefered(const std::string& path)
	{
	#ifdef WIN32
		std::string pathStr = path;
		size_t      offset = std::string::npos;

		while ((offset = pathStr.find('/')) != std::string::npos)
			pathStr.replace(offset, 1, "\\");

		return pathStr;
	#else
		return path;
	#endif
	}


	// plaform-specific escape path function
	// on windows: just puts the path in quotes
	// everything else: assume bash and escape special characters with backslashes
	std::string escape_path(const std::string& path)
	{
#ifdef WIN32

		std::string pathStr = path;
		size_t      offset = std::string::npos;

		while ((offset = pathStr.find('/')) != std::string::npos)
			pathStr.replace(offset, 1, "\\");

		// windows escapes stuff by just putting everything in quotes
		return '"' + pathStr + '"';
#else
		// a quick and dirty way to insert a backslash before most characters that would mess up a bash path
		std::string pathStr = path;

		const char *invalidChars = " '\"\\!$^&*(){}[]?;<>";
		for (unsigned int i = 0; i < pathStr.length(); i++)
		{
			char c;
			unsigned int charNum = 0;
			do
			{
				c = invalidChars[charNum];
				if (pathStr[i] == c)
				{
					pathStr.insert(i, "\\");
					i++;
					break;
				}
				charNum++;
			} while (c != '\0');
		}

		return pathStr;
#endif
	}



}