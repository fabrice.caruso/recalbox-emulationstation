#pragma once

#include <string>

#ifdef _WIN32

	#ifdef __cplusplus
	extern "C" {
	#endif	

	#include <windows.h>
	#include <process.h>
	#include <errno.h>

		typedef struct pthread_tag {
			HANDLE handle;
		} pthread_t;

		typedef CONDITION_VARIABLE pthread_cond_t;
		typedef CRITICAL_SECTION pthread_mutex_t;

		typedef struct pthread_attr_tag {
			int attr;
		} pthread_attr_t;

		typedef struct pthread_mutexattr_tag {
			int attr;
		} pthread_mutexattr_t;

		typedef DWORD pthread_key_t;

		/* ignore attribute */
		int pthread_create(pthread_t *thread, const pthread_attr_t *attr, void
			*(*start_routine)(void *), void *arg);

		void pthread_setname_np(pthread_t dwThreadID, const char* threadName);

		/* ignore value_ptr */
		void pthread_exit(void *value_ptr);

		/* ignore value_ptr */
		int pthread_join(pthread_t thread, void **value_ptr);

		pthread_t pthread_self(void);

		/* do nothing */
		int pthread_detach(pthread_t thread);

		/* DO NOT USE */
		int pthread_cancel(pthread_t thread);

		int pthread_mutexattr_destroy(pthread_mutexattr_t *attr); /* do nothing */
		int pthread_mutexattr_init(pthread_mutexattr_t *attr); /* do nothing */

		int pthread_mutex_destroy(pthread_mutex_t *mutex);
		int pthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *attr);
		int pthread_mutex_lock(pthread_mutex_t *mutex);
		//int pthread_mutex_trylock(pthread_mutex_t *mutex);
		int pthread_mutex_unlock(pthread_mutex_t *mutex);

		/* ignore deconstructor */
		int pthread_key_create(pthread_key_t *key, void(*destr_function) (void *));
		int pthread_key_delete(pthread_key_t key);
		int pthread_setspecific(pthread_key_t key, const void *pointer);
		void * pthread_getspecific(pthread_key_t key);

		int pthread_cond_destroy(pthread_cond_t *cond);
		int pthread_cond_init(pthread_cond_t *cond, const void *unused_attr);
		int pthread_cond_wait(pthread_cond_t *cond, pthread_mutex_t *mutex);
		int pthread_cond_signal(pthread_cond_t *cond);
		int pthread_cond_timedwait(pthread_cond_t* cond, pthread_mutex_t* mutex, DWORD abstime);

	//#define sleep(num) Sleep(1000*(num))

	#ifdef __cplusplus
	}
	#endif

#else
#include <unistd.h>
#include "pthread.h"
#endif

typedef pthread_t ThreadHandle;

/*!
 * Abstract class which creates, starts and controls threads.
 * This class is a simple abstraction for the concept of threads and provides
 * common functionality on disparate operating environments.
 */
class Thread
{
  public:
    /*!
     * Creates a new IThread.
     */
    Thread();

    /*!
     * Releases resources held by the IThread.
     */
    virtual ~Thread() = default;

  public:
    /*!
     * Starts the thread
     * @param name Thread's name
     */
    void Start(const std::string& name);

    /*!
     * Stops the thread
     */
    void Stop();

    /*!
     * Returns the running status of the thread
     * @return True if the thread is still running
     */
    bool IsRunning() const { return mIsRunning; }

    /*!
     * Returns the completion status of the thread
     * @return True if the thread has completed execution.
     */
    bool IsDone() const { return mIsDone; }

    static int SelfId() 
	{ 
#ifdef WIN32
		return (int) pthread_self().handle;
#else
		return (int)pthread_self(); 
#endif
	}

    /*!
     * Allow the thread to destroy blocking resources while exiting
     */
    virtual void Break() {}

    /*!
     * Static encapsulation of delay
     * @param _100thOfSecond Delay in 100th of second
     */
    static void Sleep(int milliSecond)
    {		
#ifdef _WIN32
		::Sleep(milliSecond);
#else
		usleep((useconds_t)(milliSecond * 1000));
#endif
    }

  protected:
    /*!
     * Calls the inheriting object's BeforeRun() method.
     */
    virtual void BeforeRun() {}

    /*!
     * Calls the inheriting object's Run() method.
     * @note set fIsRunning false to exit
     */
    virtual void Run() = 0;

    /*!
     * Calls the inheriting object's AfterRun() method.
     */
    virtual void AfterRun() {}

  private:
    /*!
     * Thread entry-point
     *@param thread User data (pointer to the class instance)
     */
    static void* StartThread(void* thread);

  public:

    /*!
     * Get current amount of running threads
     */
    static int GetThreadCount() { return mCount; }

  private:
    static int mCount;
    //! Thread name
    std::string mName;
    //! eCos thread handle
    ThreadHandle mHandle;
    //! Running flag
    volatile bool mIsRunning;
    //! Completion flag
    volatile bool mIsDone;
};

