#include "Timer.h"
#include <future>

Timer::Timer(int duration_in_ms, const std::function<void()>& callback)
{
	mDuration = duration_in_ms;
	mCallback = callback;
	mThread = new std::thread(&Timer::proceed, this);
}

Timer::~Timer() 
{
	mThread->join();
	delete mThread;
}

void Timer::proceed()
{
	auto ms = std::chrono::milliseconds(1);
	for (int i = 0; i < mDuration && !mCancelled; i++) {
		std::this_thread::sleep_for( ms );
	}
	if (!mCancelled)
		mCallback();
}

void Timer::clearTimeout()
{
	mCancelled = true;
}