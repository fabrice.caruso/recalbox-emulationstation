#include "Util.h"
#include "resources/ResourceManager.h"
#include "platform.h"
#include "EsLocale.h"

void strFindAndReplace(std::string& source, const std::string& find, const std::string& replace)
{
	for (size_t pos = 0; (pos = source.find(find, pos)) != std::string::npos; pos += replace.length())
	{
		source.replace(pos, find.length(), replace);
	}
}

std::string strToUpper(const char* from)
{
#ifdef WIN32
	std::wstring stringW = convertToWideString(from);

	auto& f = std::use_facet<std::ctype<wchar_t>>(std::locale());
	f.toupper(&stringW[0], &stringW[0] + stringW.size());

	return convertFromWideString(stringW);
#else
	std::string str(from);
    return boost::locale::to_upper(str);
#endif
}

std::string& strToUpper(std::string& from)
{
#ifdef WIN32
	std::wstring stringW = convertToWideString(from);

	auto& f = std::use_facet<std::ctype<wchar_t>>(std::locale());
	f.toupper(&stringW[0], &stringW[0] + stringW.size());

	from = convertFromWideString(stringW);
	return from;
#else
	from = boost::locale::to_upper(from);
    return from;
#endif
}

std::string strToUpper(const std::string& from)
{
#ifdef WIN32
	std::wstring stringW = convertToWideString(from);

	auto& f = std::use_facet<std::ctype<wchar_t>>(std::locale());
	f.toupper(&stringW[0], &stringW[0] + stringW.size());

	return convertFromWideString(stringW);
#else
	std::string str(from);
    return boost::locale::to_upper(str);
#endif
}

Transform4x4f& roundMatrix(Transform4x4f& mat)
{
	mat.translation()[0] = round(mat.translation()[0]);
	mat.translation()[1] = round(mat.translation()[1]);
	return mat;
}

Transform4x4f roundMatrix(const Transform4x4f& mat)
{
	Transform4x4f ret = mat;
	roundMatrix(ret);
	return ret;
}

Vector3f roundVector(const Vector3f& vec)
{
	Vector3f ret = vec;
	ret[0] = round(ret[0]);
	ret[1] = round(ret[1]);
	ret[2] = round(ret[2]);
	return ret;
}

Vector2f roundVector(const Vector2f& vec)
{
	Vector2f ret = vec;
	ret[0] = round(ret[0]);
	ret[1] = round(ret[1]);
	return ret;
}

std::string getExpandedPath(const std::string& str)
{
	std::string path = str;

	//expand home symbol if the startpath contains ~
	if (!path.empty() && path[0] == '~')
	{
		path.erase(0, 1);
		path.insert(0, getHomePath());
	}

	return path;
}

// expands "./my/path.sfc" to "[relativeTo]/my/path.sfc"
// if allowHome is true, also expands "~/my/path.sfc" to "/home/pi/my/path.sfc"
/*
fs::path resolvePath(const fs::path& path, const fs::path& relativeTo, bool allowHome)
{
	// nothing here
	if(path.begin() == path.end())
		return path;

	if(*path.begin() == ".")
	{
		fs::path ret = relativeTo;
		for (auto it = ++path.begin(); it != path.end(); ++it)
			ret /= *it;
		return ret;
	}

	if(allowHome && *path.begin() == "~")
	{
		fs::path ret = getHomePath();
		for (auto it = ++path.begin(); it != path.end(); ++it)
			ret /= *it;
		return ret;
	}

	return path;
}

fs::path removeCommonPathUsingStrings(const fs::path& path, const fs::path& relativeTo, bool& contains)
{
	std::string pathStr = path.generic_string();
	std::string relativeToStr = relativeTo.generic_string();
	if (pathStr.compare(0, relativeToStr.length(), relativeToStr) == 0)
	{
		contains = true;
		return pathStr.substr(relativeToStr.size() + 1);
	}
	else
	{
		contains = false;
		return path;
	}
}

// example: removeCommonPath("/home/pi/roms/nes/foo/bar.nes", "/home/pi/roms/nes/") returns "foo/bar.nes"
fs::path removeCommonPath(const fs::path& path, const fs::path& relativeTo, bool& contains)
{
	// if either of these doesn't exist, fs::canonical() is going to throw an error
	if(!fs::exists(path) || !fs::exists(relativeTo))
	{
		contains = false;
		return path;
	}

	fs::path p = fs::canonical(path);
	fs::path r = fs::canonical(relativeTo);

	if(p.root_path() != r.root_path())
	{
		contains = false;
		return p;
	}

	fs::path result;

	// find point of divergence
	auto itr_path = p.begin();
	auto itr_relative_to = r.begin();
	while(itr_path != p.end() && itr_relative_to != r.end())
		if (*itr_path == *itr_relative_to)
		{
			++itr_path;
			++itr_relative_to;
		}
		else
			break;

	if(itr_relative_to != r.end())
	{
		contains = false;
		return p;
	}

	while(itr_path != p.end())
	{
		if(*itr_path != fs::path("."))
			result = result / *itr_path;

		++itr_path;
	}

	contains = true;
	return result;
}

// usage: makeRelativePath("/path/to/my/thing.sfc", "/path/to") -> "./my/thing.sfc"
// usage: makeRelativePath("/home/pi/my/thing.sfc", "/path/to", true) -> "~/my/thing.sfc"
fs::path makeRelativePath(const fs::path& path, const fs::path& relativeTo, bool allowHome)
{
	bool contains = false;

	fs::path ret = removeCommonPath(path, relativeTo, contains);
	if(contains)
	{
		// success
		ret = "." / ret;
		return ret;
	}

	if(allowHome)
	{
		ret = removeCommonPath(path, getHomePath(), contains);
		if(contains)
		{
			// success
			ret = "~" / ret;
			return ret;
		}
	}

	// nothing could be resolved
	return path;
}
*/
boost::posix_time::ptime string_to_ptime(const std::string& str, const std::string& fmt)
{
	std::istringstream ss(str);
	ss.imbue(std::locale(std::locale::classic(), new boost::posix_time::time_input_facet(fmt))); //std::locale handles deleting the facet
	boost::posix_time::ptime time;
	ss >> time;

	return time;
}

#ifdef WIN32
#include <time.h>
#include <Windows.h>
#include <stdint.h> // portable: uint64_t   MSVC: __int64 
#include <tlhelp32.h>

int gettimeofday(struct timeval * tp, struct timezone * tzp)
{
	// Note: some broken versions only have 8 trailing zero's, the correct epoch has 9 trailing zero's
	// This magic number is the number of 100 nanosecond intervals since January 1, 1601 (UTC)
	// until 00:00:00 January 1, 1970 
	static const uint64_t EPOCH = ((uint64_t)116444736000000000ULL);

	SYSTEMTIME  system_time;
	FILETIME    file_time;
	uint64_t    time;

	GetSystemTime(&system_time);
	SystemTimeToFileTime(&system_time, &file_time);
	time = ((uint64_t)file_time.dwLowDateTime);
	time += ((uint64_t)file_time.dwHighDateTime) << 32;

	tp->tv_sec = (long)((time - EPOCH) / 10000000L);
	tp->tv_usec = (long)(system_time.wMilliseconds * 1000);
	return 0;
}

int clock_gettime(int, struct timespec *spec)
{
	__int64 wintime; GetSystemTimeAsFileTime((FILETIME*)&wintime);
	wintime -= 116444736000000000i64;  //1jan1601 to 1jan1970
	spec->tv_sec = wintime / 10000000i64;           //seconds
	spec->tv_nsec = wintime % 10000000i64 * 100;      //nano-seconds
	return 0;
}


DWORD getppid()
{
	HANDLE hSnapshot = INVALID_HANDLE_VALUE;
	PROCESSENTRY32 pe32;
	DWORD ppid = 0, pid = GetCurrentProcessId();

	hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	__try {
		if (hSnapshot == INVALID_HANDLE_VALUE) __leave;

		ZeroMemory(&pe32, sizeof(pe32));
		pe32.dwSize = sizeof(pe32);
		if (!Process32First(hSnapshot, &pe32)) __leave;

		do {
			if (pe32.th32ProcessID == pid) {
				ppid = pe32.th32ParentProcessID;
				break;
			}
		} while (Process32Next(hSnapshot, &pe32));

	}
	__finally {
		if (hSnapshot != INVALID_HANDLE_VALUE) CloseHandle(hSnapshot);
	}
	return ppid;
}
#endif