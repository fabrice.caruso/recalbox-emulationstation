#include "RootFolders.h"

#ifdef WIN32
#include "platform.h"

const std::string RootFolders::TemplateRootFolder = getHomePath();
const std::string RootFolders::DataRootFolder = getHomePath();
const std::string RootFolders::EmulationStationFolder = "/.emulationstation";
#else
const std::string RootFolders::TemplateRootFolder = "/recalbox/share_init";
const std::string RootFolders::DataRootFolder = "/recalbox/share";
const std::string RootFolders::EmulationStationFolder = "/system/.emulationstation";
#endif
