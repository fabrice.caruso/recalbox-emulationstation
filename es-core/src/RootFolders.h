#pragma once

#include <string>

class RootFolders
{
  public:
    static const std::string TemplateRootFolder; // /recalbox/share_init
    static const std::string DataRootFolder;     // /recalbox/share
	static const std::string EmulationStationFolder;     // /system/.emulationstation/
};

