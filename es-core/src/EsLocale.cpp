#include "EsLocale.h"

#ifdef WIN32
#include <Windows.h>

const std::wstring convertToWideString(const std::string string)
{
	int numBytes = MultiByteToWideChar(CP_UTF8, 0, string.c_str(), (int)string.length(), nullptr, 0);

	std::wstring wstring;
	wstring.resize(numBytes);
	MultiByteToWideChar(CP_UTF8, 0, string.c_str(), (int)string.length(), (WCHAR*)wstring.c_str(), numBytes);

	return wstring;
}

const std::string convertFromWideString(const std::wstring wstring)
{
	int numBytes = WideCharToMultiByte(CP_UTF8, 0, wstring.c_str(), (int)wstring.length(), nullptr, 0, nullptr, nullptr);

	std::string string;
	string.resize(numBytes);
	WideCharToMultiByte(CP_UTF8, 0, wstring.c_str(), (int)wstring.length(), (char*)string.c_str(), numBytes, nullptr, nullptr);

	return string;
}
#endif