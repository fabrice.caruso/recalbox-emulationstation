#include "Gamelist.h"
#include "SystemData.h"
#include <boost/property_tree/xml_parser.hpp>
#include <RecalboxConf.h>
#include "Log.h"
#include "Settings.h"
#include "Util.h"
#include "recalbox/RecalboxSystem.h"
#include "util/FileSystem.h"

namespace pt = boost::property_tree;

FileData* findOrCreateFile(SystemData* system, const std::string& path, ItemType type, bool trustGamelist, FileData::StringMap& doppelgangerWatcher)
{
	// first, verify that path is within the system's root folder
	FolderData* root = system->getRootFolder();

	bool contains = false;
	std::string relativepath = trustGamelist ? FileSystem::removeCommonPathUsingStrings(path, root->getPath(), contains) : FileSystem::removeCommonPath(path, root->getPath(), contains);
	if (!contains)
	{
		LOG(LogError) << "File path \"" << path << "\" is outside system path \"" << system->getStartPath() << "\"";
		return nullptr;
	}


	auto relative = FileSystem::get_split_path(relativepath);

	auto path_it = relative.begin();
	FolderData* treeNode = root;
	std::string key;
	while (path_it != relative.end())
	{
		// Get the key for duplicate detection. MUST MATCH KEYS USED IN populateRecursiveFolder.populateRecursiveFolder - Always fullpath
		key = (FileSystem::combine(treeNode->getPath(), *path_it));
		FileData* item = (doppelgangerWatcher.find(key) != doppelgangerWatcher.end()) ? doppelgangerWatcher[key] : nullptr;

		// Last path component?
		if (path_it == --relative.end())
		{
			if (type == ItemType::Game) // Final file
			{
				FileData* game = item;
				if (game == nullptr)
				{
					// Add final game
					game = new FileData(path, system);
					doppelgangerWatcher[key] = game;
					treeNode->addChild(game, true);
				}
				return game;
			}
			else // Final folder (scrapped obviously)
			{
				FolderData* folder = static_cast<FolderData*>(item);
				if (folder == nullptr)
				{
					// create missing folder
					folder = new FolderData(key, system);
					doppelgangerWatcher[key] = folder;
					treeNode->addChild(folder, true);
				}
				return folder;
			}
		}
		else // Intermediate path
		{
			FolderData* folder = static_cast<FolderData*>(item);
			if (folder == nullptr)
			{
				// create missing folder
				folder = new FolderData(key, system);
				doppelgangerWatcher[key] = folder;
				treeNode->addChild(folder, true);
			}
			treeNode = folder;
		}
		path_it++;
	}

	return nullptr;
}

void parseGamelist(SystemData* system, FileData::StringMap& doppelgangerWatcher)
{
	try
	{
		bool trustGamelist = RecalboxConf::getInstance()->get("emulationstation.gamelistonly") == "1";
		std::string xmlpath = system->getGamelistPath(false);

		if (!FileSystem::exists(xmlpath))
			return;

		LOG(LogInfo) << "Parsing XML file \"" << xmlpath << "\"...";

		MetadataDescriptor::Tree gameList;
		try
		{
#if _MSC_VER < 1910
			pt::read_xml(xmlpath, gameList, 0);
#else
			pt::read_xml(xmlpath, gameList, 0, std::locale("en_US.UTF8"));
#endif
		}
		catch (std::exception& e)
		{
			LOG(LogError) << "Could not parse " << xmlpath << " file!";
			LOG(LogError) << e.what();
			return;
		}

		std::string relativeTo = system->getStartPath();
		std::string path;
		for (const auto& fileNode : gameList.get_child("gameList"))
		{
			ItemType type;
			if (fileNode.first == "game") type = ItemType::Game;
			else if (fileNode.first == "folder") type = ItemType::Folder;
			else continue; // Unknown node

			const MetadataDescriptor::Tree& children = fileNode.second;
			path = FileSystem::resolveRelativePath(children.get("path", ""), relativeTo, false);

			if (!trustGamelist && !FileSystem::exists(path))
			{
				LOG(LogWarning) << "File \"" << path << "\" does not exist! Ignoring.";
				continue;
			}

			FileData* file = findOrCreateFile(system, path, type, trustGamelist, doppelgangerWatcher);
			if (!file)
			{
				LOG(LogError) << "Error finding/creating FileData for \"" << path << "\", skipping.";
				continue;
			}

			//load the metadata
			file->Metadata().Deserialize(fileNode, FileSystem::get_generic_path(relativeTo));
		}
	}
	catch (std::exception& ex)
	{
		LOG(LogError) << "System \"" << system->getName() << "\" has raised an error while reading its gamelist.xml!";
		LOG(LogError) << "Exception: " << ex.what();
	}
}

void updateGamelist(SystemData* system)
{
	//We do this by reading the XML again, adding changes and then writing it back,
	//because there might be information missing in our systemdata which would then miss in the new XML.
	//We have the complete information for every game though, so we can simply remove a game
	//we already have in the system from the XML, and then add it back from its GameData information...
	if (Settings::getInstance()->getBool("IgnoreGamelist")) return;

	try
	{
		/*
		 * Get all folder & games in a flat storage
		 */
		FolderData *rootFolder = system->getRootFolder();
		if (rootFolder == nullptr) return;

		FileData::List fileData = rootFolder->getAllItemsRecursively(true);
		// Nothing to process?
		if (fileData.empty()) return;

		/*
		 * Create game/folder map for fast seeking using relative path as key
		 */
		std::unordered_map<std::string, const FileData *> fileLinks;
		for (const FileData *file : fileData)                                                                    // For each File
			if (file->Metadata().IsDirty())                                                                        // with updated metadata
				fileLinks[FileSystem::makeRelativePath(file->getPath(), system->getStartPath(), false)] = file; // store the relative path
			// Nothing changed?
		if (fileLinks.empty()) return;

		/*
		 * Load or create gamelist node
		 */
		std::string xmlReadPath = system->getGamelistPath(false);
		MetadataDescriptor::Tree document;
		if (FileSystem::exists(xmlReadPath))
		{
			try
			{
#if WIN32 && _MSC_VER < 1910
				pt::read_xml(xmlReadPath, document, pt::xml_parser::trim_whitespace);
#else
				pt::read_xml(xmlReadPath, document, pt::xml_parser::trim_whitespace, std::locale("en_US.UTF8"));
#endif
			}
			catch (std::exception &e)
			{
				LOG(LogError) << "Could not parse " << xmlReadPath << " file!";
				LOG(LogError) << e.what();
			}
		}
		else
		{
			// Create empty game list node
			document.add_child("gameList", MetadataDescriptor::Tree());
		}
		MetadataDescriptor::Tree &gameList = document.get_child("gameList");

		/*
		 * Update pass #1 : Remove node from the gamelist where corresponding metadata have changed
		 */
		for (auto it = gameList.begin(); it != gameList.end();)                         // For each gamelist entry
			if (fileLinks.find((*it).second.get("path", "")) != fileLinks.end())          // corresponding to an updated file
				it = gameList.erase(it);                                                    // delete the entry from the gamelist
			else
				++it;

		/*
		 * Update pass #2 - Insert new/updated game/folder nodes into the gamelist node
		 */
		for (const FileData *file : fileData)                                 // For each file
			if (file->Metadata().IsDirty())                                     // If metadata have changed
				file->Metadata().Serialize(gameList,                              // Insert updated node
					FileSystem::get_generic_path(file->getPath()),
					system->getStartPath());

		/*
		 * Write the list.
		 * At this point, we're sure at least one node has been updated (or added and updated).
		 */

		std::string xmlWritePath = system->getGamelistPath(true);
		FileSystem::create_directory(FileSystem::get_parent_path(xmlWritePath));

		try
		{
			pt::xml_writer_settings<std::string> settings(' ', 2);

#if WIN32 && _MSC_VER < 1910
			pt::write_xml(xmlWritePath, document, std::locale(), settings);
#else
			pt::write_xml(xmlWritePath, document, std::locale("en_US.UTF8"), settings);
#endif
			LOG(LogInfo) << "Saved gamelist.xml for system " << system->getFullName() << ". Updated items: " << fileLinks.size() << "/" << fileData.size();
		}
		catch (std::exception &e)
		{
			LOG(LogError) << "Failed to save " << xmlWritePath << " : " << e.what();
		}

	}
	catch (std::exception& e)
	{
		LOG(LogError) << "Something went wrong while saving " << system->getFullName() << " : " << e.what();
	}
}