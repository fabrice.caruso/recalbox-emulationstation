#include <RecalboxConf.h>
#include "components/TextListComponent.h"
#include "components/IList.h"
#include "views/gamelist/BasicGameListView.h"
#include "views/ViewController.h"
#include "Renderer.h"
#include "Window.h"
#include "ThemeData.h"
#include "SystemData.h"
#include "FileSorts.h"
#include "Settings.h"
#include "EsLocale.h"
#include <boost/assign.hpp>
#include <recalbox/RecalboxSystem.h>

static const std::map<std::string, const UNICODE_CHARTYPE> favorites_icons_map {
		{ "snes"        , UNICODE_CHARS("\uF25e ") },
		{ "3do"         , UNICODE_CHARS("\uF28a ") },
		{ "x68000"      , UNICODE_CHARS("\uF28b ") },
		{ "amiga600"    , UNICODE_CHARS("\uF244 ") },
		{ "amiga1200"   , UNICODE_CHARS("\uF245 ") },
		{ "nds"         , UNICODE_CHARS("\uF267 ") },
		{ "c64"         , UNICODE_CHARS("\uF24c ") },
		{ "nes"         , UNICODE_CHARS("\uF25c ") },
		{ "n64"         , UNICODE_CHARS("\uF260 ") },
		{ "gba"         , UNICODE_CHARS("\uF266 ") },
		{ "gbc"         , UNICODE_CHARS("\uF265 ") },
		{ "gb"          , UNICODE_CHARS("\uF264 ") },
		{ "fds"         , UNICODE_CHARS("\uF25d ") },
		{ "virtualboy"  , UNICODE_CHARS("\uF25f ") },
		{ "gw"          , UNICODE_CHARS("\uF278 ") },
		{ "dreamcast"   , UNICODE_CHARS("\uF26e ") },
		{ "megadrive"   , UNICODE_CHARS("\uF26b ") },
		{ "segacd"      , UNICODE_CHARS("\uF26d ") },
		{ "sega32x"     , UNICODE_CHARS("\uF26c ") },
		{ "mastersystem", UNICODE_CHARS("\uF26a ") },
		{ "gamegear"    , UNICODE_CHARS("\uF26f ") },
		{ "sg1000"      , UNICODE_CHARS("\uF269 ") },
		{ "psp"         , UNICODE_CHARS("\uF274 ") },
		{ "psx"         , UNICODE_CHARS("\uF275 ") },
		{ "pcengine"    , UNICODE_CHARS("\uF271 ") },
		{ "pcenginecd"  , UNICODE_CHARS("\uF273 ") },
		{ "supergrafx"  , UNICODE_CHARS("\uF272 ") },
		{ "scummvm"     , UNICODE_CHARS("\uF27a ") },
		{ "dos"         , UNICODE_CHARS("\uF24a ") },
		{ "fba"         , UNICODE_CHARS("\uF252 ") },
		{ "fba_libretro", UNICODE_CHARS("\uF253 ") },
		{ "mame"        , UNICODE_CHARS("\uF255 ") },
		{ "neogeo"      , UNICODE_CHARS("\uF257 ") },
		{ "colecovision", UNICODE_CHARS("\uF23f ") },
		{ "atari2600"   , UNICODE_CHARS("\uF23c ") },
		{ "atari7800"   , UNICODE_CHARS("\uF23e ") },
		{ "lynx"        , UNICODE_CHARS("\uF270 ") },
		{ "ngp"         , UNICODE_CHARS("\uF258 ") },
		{ "ngpc"        , UNICODE_CHARS("\uF259 ") },
		{ "wswan"       , UNICODE_CHARS("\uF25a ") },
		{ "wswanc"      , UNICODE_CHARS("\uF25b ") },
		{ "prboom"      , UNICODE_CHARS("\uF277 ") },
		{ "vectrex"     , UNICODE_CHARS("\uF240 ") },
		{ "lutro"       , UNICODE_CHARS("\uF27d ") },
		{ "cavestory"   , UNICODE_CHARS("\uF276 ") },
		{ "atarist"     , UNICODE_CHARS("\uF248 ") },
		{ "amstradcpc"  , UNICODE_CHARS("\uF246 ") },
		{ "msx"         , UNICODE_CHARS("\uF24d ") },
		{ "msx1"        , UNICODE_CHARS("\uF24e ") },
		{ "msx2"        , UNICODE_CHARS("\uF24f ") },
		{ "odyssey2"    , UNICODE_CHARS("\uF241 ") },
		{ "zx81"        , UNICODE_CHARS("\uF250 ") },
		{ "zxspectrum"  , UNICODE_CHARS("\uF251 ") },
		{ "moonlight"   , UNICODE_CHARS("\uF27e ") },
		{ "apple2"      , UNICODE_CHARS("\uF247 ") },
		{ "gamecube"    , UNICODE_CHARS("\uF262 ") },
		{ "wii"         , UNICODE_CHARS("\uF263 ") },
		{ "imageviewer" , UNICODE_CHARS("\uF27b ") } };

BasicGameListView::BasicGameListView(Window* window, FolderData* root)
	: ISimpleGameListView(window, root),
	  mList(window),
    mEmptyListItem(root->getSystem()),
	  listingOffset(0)
{
	mList.setSize(mSize.x(), mSize.y() * 0.8f);
	mList.setPosition(0, mSize.y() * 0.2f);
	mList.setDefaultZIndex(20);
	addChild(&mList);

	mEmptyListItem.Metadata().SetName(_("EMPTY LIST"));

	populateList(root);
}

void BasicGameListView::onThemeChanged(const std::shared_ptr<ThemeData>& theme)
{
	ISimpleGameListView::onThemeChanged(theme);
	using namespace ThemeFlags;
	mList.applyTheme(theme, getName(), "gamelist", ALL);
	sortChildren();
}

void BasicGameListView::onFileChanged(FileData* file, FileChangeType change)
{
	ISimpleGameListView::onFileChanged(file, change);

	if(change == FileChangeType::MetadataChanged)
	{
		// might switch to a detailed view
		ViewController::get()->reloadGameListView(this);
		return;
	}
}


const std::string BasicGameListView::getItemIcon(FileData* item)
{
	UNICODE_CHARTYPE ret = nullptr;

	if (item->Metadata().Hidden()) 
		ret = UNICODE_CHARS("\uF070 ");
	else if ((item->isGame()) && (item->Metadata().Favorite()))
	{
		auto icon = favorites_icons_map.find(item->getSystem()->getName());

		if ((icon != favorites_icons_map.end()))
			ret = (UNICODE_CHARTYPE) (*icon).second;
		else 
			ret = UNICODE_CHARS("\uF006 ");		
	}

	if (ret != nullptr)
	{
#ifdef WIN32
		std::wstring toConvert = ret;
		const std::string converted = convertFromWideString(toConvert);
		return converted.c_str();
#else
		std::string value = ret;
		return value;
#endif
	}

	return "";
}

void BasicGameListView::populateList(const FolderData* folder)
{
	mPopulatedFolder = folder;
	mList.clear();
	mHeaderText.setText(mSystem->getFullName());

  // Default filter
  FileData::Filter filter = FileData::Filter::Normal | FileData::Filter::Favorite;
  // Add hidden?
  if (Settings::getInstance()->getBool("ShowHidden"))
    filter |= FileData::Filter::Hidden;
  // Favorites only?
  if (mFavoritesOnly)
    filter = FileData::Filter::Favorite;

  // Get items
  bool flatfolders = (RecalboxConf::getInstance()->getBool(getRoot()->getSystem()->getName() + ".flatfolder"));
  int count = flatfolders ? folder->countFilteredItemsRecursively(filter, false) : folder->countFilteredItems(filter, true);
  if (count == 0) filter |= FileData::Filter::Normal;
	FileData::List items = flatfolders ? folder->getFilteredItemsRecursively(filter, false) : folder->getFilteredItems(filter, true);
	// Check emptyness
	if (items.empty())
  {
	  // Insert "EMPTY SYSTEM" item
    items.push_back(&mEmptyListItem);
  }

  // Sort
  const FileSorts::SortType& sortType = mSystem->getSortType(mSystem->isFavorite());
  FolderData::Sort(items, sortType.comparisonFunction, sortType.ascending);

  // Add to list
  //mList.reserve(items.size()); // TODO: Reserve memory once
  for (FileData* fd : items)
	{
  	// Get name
  	std::string name = fd->getName();
  	// Select fron icon
    auto icon = getItemIcon(fd);
    if (!icon.empty())
    	name = icon + name;
    // Store
		mList.add(name, fd, fd->isFolder() ? 1 : 0, false);
	}
}

void BasicGameListView::refreshList()
{
    populateList(mPopulatedFolder);
}

FileData::List BasicGameListView::getFileDataList()
{
	FileData::List objects = mList.getObjects();
	FileData::List slice;
  for (auto it = objects.begin() + listingOffset; it != objects.end(); it++)
  {
    slice.push_back(*it);
  }
  return slice;
}

FileData* BasicGameListView::getCursor() {
	return mList.getSelected();
}

void BasicGameListView::setCursorIndex(int index)
{
  if (index >= mList.size()) index = mList.size() - 1;
  if (index < 0) index = 0;

  RecalboxSystem::getInstance()->NotifyGame(*getCursor(), false, false);
	mList.setCursorIndex(index);
}

int BasicGameListView::getCursorIndex()
{
  return mList.getCursorIndex();
}

int BasicGameListView::getCursorIndexMax(){
	return mList.size() - 1;
}

void BasicGameListView::setCursor(FileData* cursor)
{
	if(!mList.setCursor(cursor, listingOffset))
	{
		populateList(mRoot);
		mList.setCursor(cursor);

		// update our cursor stack in case our cursor just got set to some folder we weren't in before
		if(mCursorStack.empty() || mCursorStack.top() != cursor->getParent())
		{
			std::stack<FolderData*> tmp;
			FolderData* ptr = cursor->getParent();
			while(ptr && ptr != mRoot)
			{
				tmp.push(ptr);
				ptr = ptr->getParent();
			}
			
			// flip the stack and put it in mCursorStack
			mCursorStack = std::stack<FolderData*>();
			while(!tmp.empty())
			{
				mCursorStack.push(tmp.top());
				tmp.pop();
			}
		}
	}
  RecalboxSystem::getInstance()->NotifyGame(*getCursor(), false, false);
}

void BasicGameListView::launch(FileData* game) {
	ViewController::get()->launch(game);
}