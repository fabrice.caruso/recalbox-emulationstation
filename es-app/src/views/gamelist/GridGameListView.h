#pragma once

#include "views/gamelist/ISimpleGameListView.h"
#include "components/ImageGridComponent.h"
#include "components/ImageComponent.h"
#include <stack>
#include "EmptyData.h"

class GridGameListView : public ISimpleGameListView
{
public:
	GridGameListView(Window* window, FolderData* root);

	//virtual void onThemeChanged(const std::shared_ptr<ThemeData>& theme) override;

	virtual FileData* getCursor() override;
	virtual void setCursor(FileData*) override;

	virtual bool input(InputConfig* config, Input input) override;

	virtual const char* getName() const override { return "grid"; }

	virtual std::vector<HelpPrompt> getHelpPrompts() override;

	virtual int getCursorIndex() override;
	virtual int getCursorIndexMax() override;
	virtual void setCursorIndex(int index) override;
	virtual FileData::List getFileDataList();

protected:
	EmptyData mEmptyListItem;

	virtual void populateList(const FolderData* folder) override;
	virtual void launch(FileData* game) override;

	virtual FileData* getEmptyListItem() override { return &mEmptyListItem; }

	ImageGridComponent<FileData*> mGrid;
};
